import { projectDbFields } from "./config.js";

// Uses a geojson to create a card html for each feature entry.
// Adds an event to center a leaflet map when the card is clicked.
// Adds icons to the card to show the secondary taxonomy
// Params:
//     map: the leaflet map instance
//     geojson: the geojson used to add markers on the map
//     taxonomies: the object with the necessary info to display
export function createCards(map, geojson, taxonomies) {
    
    // Declaro variable cards que será el div que hemos creado arriba 
    // cuyo id es cards, o sea, la "tarjeta" que muestra toda la info 
    // de los colectivos
    var cards = document.querySelector('#cards');
    
    // Apaño para deslocalizadas
    geojson.features.forEach((feature) => {
        
        var cardEl = document.createElement('div');
        cardEl.setAttribute('id', "card_" + feature.properties.id);
        cardEl.setAttribute('class', 'card');
        var lat = feature.geometry.coordinates[1];
        var lng = feature.geometry.coordinates[0];
        cardEl.setAttribute('data-lat', lat);
        cardEl.setAttribute('data-lng', lng);
        
        var project = feature.properties;
        
        cardEl.setAttribute(`data-${taxonomies.main.singleId}`, project[taxonomies.main.dbName]);
        cardEl.setAttribute(`data-${taxonomies.secondary.singleId}`, project[taxonomies.secondary.dbName]);
        
        const secondaryTaxonomyIconRow = (category) => {
            return `
            <div class="secondary-taxonomy-icon-row">
            <i>
            <img src="assets/img/categories/${taxonomies.secondary.icons[category]}">
            </i>
            </div>
            `
        }
        cardEl.innerHTML = `
        <h3>${project[projectDbFields.name]}</h3>
        <h4>${project[projectDbFields.place] == null ? `Sin localizar` : project[projectDbFields.place]}</h4>
        ${project[taxonomies.secondary.dbName] ? secondaryTaxonomyIconRow(project[taxonomies.secondary.dbName]) : null }
        <p>
        ${project[projectDbFields.description]}
        </p>
        <button class="close-btn">
        <img src="assets/img/cancel.png">
        </button>
        `
        
        // Cuando clico sobre la card la vista se dirige a esas latitudes
        if (project[projectDbFields.place] != null) {
            cardEl.addEventListener('click', (event) => {
                var latlng = L.latLng(event.target.dataset.lat, event.target.dataset.lng);
                map.setView(latlng, 15);
                unselectCards();
                event.target.classList.add('selected');
            });
        }
        // Añado escucha de evento para cerrar tarjeta
        cardEl.querySelector('.close-btn').addEventListener('click', (event) =>{
            // Evitamos que el evento se propage hacia la tarjeta
            event.stopPropagation()
            hideCards()
        })
        
        cards.appendChild(cardEl);
    })
    
}

export function selectCard(id) {
    // Elimino la selección anterior
    unselectCards();
    // Si no se ha seleccinoado una tarjeta oculto la vista de tarjetas
    if (id == null) {
        hideCards();
        return;
    }
    
    // Muestro las tarjetas
    showCards()
    // Centro la vista de tarjetas en la tarjeta seleccionada
    var cardEl = document.querySelector(`#card_${id}`);
    if (cardEl) {
        cardEl.classList.add('selected');
        cardEl.scrollIntoView({
            block: "center",
            inline: "center",
            behavior: "smooth"
        })
    }
}

export function unselectCards() {
    document.querySelectorAll(".selected").forEach(card => {
        card.classList.remove('selected')
    })
}

export function hideCards() {
    document.querySelector("#cards").classList.add('hidden');
}

export function showCards() {
    document.querySelector("#cards").classList.remove('hidden');
}

