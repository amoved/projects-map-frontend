import { taxonomies } from "./config.js"


export function initFilter(taxonomies) {

    let filter = {
        search: ""
    }
    filter[taxonomies.main.allId] = []
    filter[taxonomies.secondary.allId] = []

    return filter
}

// Global functions
export function setFilter(filter, taxonomies, logs = false) {
    
    Object.keys(taxonomies).forEach(key => {
        
        var selectedCategories = document.querySelectorAll(`.${taxonomies[key].singleId}:checked`);
        filter[taxonomies[key].allId] = [];
        if(selectedCategories.length == 0){
            document.querySelector(`#legend-${taxonomies[key].priority}-taxonomy`).classList.add('show-all')
            return
        } else {
            selectedCategories.forEach(checkbox => {
                filter[taxonomies[key].allId].push(checkbox.name)
            })
            document.querySelector(`#legend-${taxonomies[key].priority}-taxonomy`).classList.remove('show-all')
        }
        
    })
    
    if(logs){
        console.log('-------------------------------')
        console.log('---------Nuevo filtro----------')
        console.log('-------------------------------')
        console.log(filter)
    }
    
}

// Comprobamos la existencia de uno o varios valores separados por ';' en un filtro
function filterVal(filter, val, logs = false) {
    if(logs){
        console.log(`Filtro de búsqueda:`)
        console.log(filter)
    }
    
    let found = false
    //console.log(`Filtrando ${val}`)
    if(val.indexOf(';') > -1) {
        // Separamos cada valor
        let vals = val.split(';')
        if(logs){
            console.log(`Valores:`)
            console.log(vals)
        }
        vals.forEach(val => {
            if(filter.indexOf(val) >= 0){
                found = true
                if(logs) {
                    console.log('Found!')
                }
            }
        })
    } else if(val){ 
        if(logs){
            console.log(`Valor:`)
            console.log(val)
        }
        return filter.indexOf(val) >= 0
    } 
    return found
}

export function filterProjectByTaxonomies(filter, project, taxonomies, logs = false) {
    
    // Fijo banderas para saber si el marcador debería mostrarse, una por cada taxonomía
    var visibleMarker = [false,false]
    
    Object.keys(taxonomies).forEach((key,i) => {
        
        // Si no hay ninguna categoría seleccionada, muestra todo
        if(filter[taxonomies[key].allId].length == 0){
            visibleMarker[i] = true
        }else if (project[taxonomies[key].singleId]) {
            visibleMarker[i] = filterVal(filter[taxonomies[key].allId],project[taxonomies[key].singleId])
        }
        
    })
    
    if(logs && visibleMarker.includes(false)){
        console.log("El filtro para la taxonomía main:")
        console.log(filter[taxonomies.main.allId])
        console.log("La categoría del proyecto:")
        console.log(project[taxonomies.main.singleId])
        
        console.log("El filtro para la taxonomía secondary:")
        console.log(filter[taxonomies.secondary.allId])
        console.log("La categoría del proyecto:")
        console.log(project[taxonomies.secondary.singleId])
        
        console.log(visibleMarker)
    }
    
    return !visibleMarker.includes(false);
}