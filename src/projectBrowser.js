import {
    createCards,
    selectCard,
    hideCards,
} from "./cards.js"
import { filterMarkers } from "./map.js"
import { 
    initFilter,
    setFilter,
    filterProjectByTaxonomies
 } from "./filter.js";

function filterCards(filter, taxonomies) {
    // Selecciono alls los elementos card
    var cards = document.querySelectorAll('.card');
    // Almaceno la cadena de caracteres del buscador
    filter.search = document.querySelector('#search-filter').value;
    // Lo transformo en minúsculas
    var filterLowercase = filter.search.toLowerCase();
    
    // let counter = 0, invCounter = 0
    
    // Itero sobre cada card
    for (var i = 0; i < cards.length; i++) {
        
        var project = {}
        project[taxonomies.main.singleId] = cards[i].dataset[taxonomies.main.singleId]
        project[taxonomies.secondary.singleId] = cards[i].dataset[taxonomies.secondary.singleId]
        var isAllowedByFilter = filterProjectByTaxonomies(filter,project,taxonomies)
        
        var title = cards[i].querySelector('h3');
        var titleLowercase = title.innerText.toLowerCase();
        
        var foundInSearch = (filterLowercase == '' || titleLowercase.indexOf(filterLowercase) >= 0);
        
        if(isAllowedByFilter && foundInSearch) {
            cards[i].style.display = 'block';
            // counter++
        } else {
            cards[i].style.display = 'none';
            // invCounter++
        }
    }
    // console.log(`Displayed cards of projects ${counter}, hidden projects ${invCounter}`)
    var cardsContainer = document.querySelector('#cards');
    cardsContainer.scrollTop = 0;
}

function onCategoryClick(map, geojson, filter, taxonomies) {
    setFilter(filter, taxonomies);
    selectCard();
    filterCards(filter, taxonomies);
    filterMarkers(map, geojson, filter, taxonomies); // mira map.js
}

// Crea el contenido de la primera columna (div#legend)
// Asigna a cada taxonomía de la leyenda el evento click -> filterLegend()
function createLegend(map, geojson, filter, taxonomies = []) {
    
    Object.keys(taxonomies).forEach(taxKey => {
        // Creo los butones (checkbox) para las categorías de cada taxonomía (filtros de la leyenda)
        var legendTaxInnerHTML = '';
        var legendTaxEl = document.querySelector(`#legend-${taxonomies[taxKey].priority}-taxonomy`);
        var className = taxonomies[taxKey].singleId
        
        Object.keys(taxonomies[taxKey].categories).forEach(catKey => {
            legendTaxInnerHTML +=
            `<input class="${className}" type="checkbox" id="${catKey}" name="${catKey}">
            <label for="${catKey}">
            <img src="assets/img/categories/${taxonomies[taxKey].icons[catKey]}" /> ${taxonomies[taxKey].categories[catKey]}
            </label>`
        })
        
        legendTaxEl.innerHTML = legendTaxInnerHTML;
        
        // Añado el evento click a la leyenda
        var categories = document.querySelectorAll(`.${className}`);
        categories.forEach(category => {
            category.addEventListener('click', () => onCategoryClick(map, geojson, filter, taxonomies));
        })
    })
    
}

function registerSearchBarEvents(filter, taxonomies){
    // Acción: mostrar cartas coincidentes
    document.querySelector('#search-button').addEventListener('click', () => {
        filterCards(filter, taxonomies)
    })
    // Acción: al pulsar enter, hace click sobre el botón de búsqueda programáticamente
    document.querySelector('#search-filter').addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.querySelector('#search-button').click();
        }
    });
    // Acción: al escribir muesrta el botón para limpiar la barra de búsqueda
    document.querySelector('#search-filter').addEventListener("input", function (event) {
        if (event.target.value == '') {
            document.querySelector('#cancel-search-button').style.visibility = "hidden";
        } else {
            document.querySelector('#cancel-search-button').style.visibility = "visible";
        }
    });
    // Acción: al pulsar el botón de limpiar la barra de búsqueda, borra el texto 
    // actual y refresca las tarjetas mostradas
    document.querySelector('#cancel-search-button').addEventListener("click", function () {
        document.querySelector('#search-filter').value = '';
        document.querySelector('#cancel-search-button').style.visibility = "hidden";
        document.querySelector('#search-button').click();
    });
}

function registerMobileButtonsEvents() {
    // Acción: al pulsar en el botón de ver mapa muestra el mapa con una animación
    document.querySelector('#viewmap-button').addEventListener('click', (e) => {
        document.querySelector('#legend').style.left = '-100%';
        document.querySelector('#viewmap').style.display = 'none';
        document.querySelector('#viewlegend').style.display = 'block';
    })
    // Acción: al pulsar en el botón de ver leyenda muestra la leyenda con una animación
    document.querySelector('#viewlegend-button').addEventListener('click', (e) => {
        document.querySelector('#legend').style.left = '0px';
        document.querySelector('#viewmap').style.display = 'block';
        document.querySelector('#viewlegend').style.display = 'none';
        hideCards();
    })
}

export function createProjectBrowser(map, geojson, taxonomies){
    
    let filter = initFilter(taxonomies);

    createLegend(map, geojson, filter, taxonomies);
    createCards(map, geojson, taxonomies);

    setFilter(filter, taxonomies);

    registerSearchBarEvents(filter, taxonomies);
    registerMobileButtonsEvents();
    
    // Ocultamos las tarjetas inicialmente para la vista movil
    if(screen.width < 1000){
        hideCards();
    }
}