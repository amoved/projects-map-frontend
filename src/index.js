import {docReady} from './utils.js'
import {createProjectBrowser} from './projectBrowser.js'
import {createMap} from './map.js'
import {
    siteUrl,
    dbPath,
    taxonomies
} from './config.js'

// Bootstrap
docReady(() => {
    // Cargar objeto geojson en entorno global
    fetch(siteUrl + dbPath, {'Content-Type':'application/json'})
        .then(
            async (response)  => {
        
                const geojsonArray = await response.json();
                const geojson = geojsonArray[0]
                
                //Creo el mapa con Leaflet
                const map = createMap(geojson);
                createProjectBrowser(map,geojson,taxonomies)
                
            }
        )
  })