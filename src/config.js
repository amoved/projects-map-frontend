export const siteUrl = 'http://localhost:5500/'
export const dbPath = 'db/proyectos.json'
 
export const taxonomies = {
    "main": {
        "priority": "main",
        "name": "Bioregión",
        "allId": "bioregiones",
        "singleId": "bioregion",
        "dbName": "Bioregion",
        "categories": {
            'centro': "Centro",
            'norte': "Norte",
            'noreste': "Noreste",
            'noroeste': "Noroeste",
            'sur': "Sur",
            'sureste': "Sureste",
            'suroeste': "Suroeste",
            'levante': "Levante",
            'baleares': "Islas Baleares",
            'canarias': "Islas Canarias"
        },
        "icons": {
            'centro': "ajo.png",
            'norte': "repollo.png",
            'noreste': "espinacas.png",
            'noroeste': "cebolla.png",
            'sur': "pepino.png",
            'sureste': "rabano.png",
            'suroeste': "pimiento-morron.png",
            'levante': "limon.png",
            'baleares': "tomate.png",
            'canarias': "maiz.png"
        }
    },
    "secondary": {
        "priority": "secondary",
        "name": "Ámbito",
        "allId": "ambitos",
        "singleId": "ambito",
        "dbName": "Ambito",
        "categories": {
            "personal": "Proyecto personal",
            "colectivo": "Proyecto colectivo",
            "educativo": "Proyecto educativo"
        },
        "icons": {
            'personal': "personal.png",
            'colectivo': "colectivo.png",
            'educativo': "educativo.png"
        }
    }
}

export const projectDbFields = {
    "name": "Nombre",
    "place": "Ciudad",
    "description": "Descripc1"
}