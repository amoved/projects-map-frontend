// Leaflet is loaded globally. This is forced since markerCluster 
// relies on global L variable.
import { filterProjectByTaxonomies } from "./filter.js";
import { selectCard } from "./cards.js";

// Global variables
let markers;

// Defino el polígono para proyectos sin ubicación
const geojsonFeaturePolygon = [{
    "type": "Feature",
    "properties": {},
    "geometry": {
        "type": "Polygon",
        "coordinates": [
            [
                [
                    -15.17,
                    40.2
                ],
                [
                    -12.9,
                    40.2
                ],
                [
                    -12.9,
                    38.4
                ],
                [
                    -15.17,
                    38.4
                ],
            ]
        ]
    }
}];

// Global functions
function stylePolygon(feature) {
    return {
        weight: 1.3, // grosor de línea
        color: 'green', // color de línea
        opacity: 1.0, // tansparencia de línea
        fillColor: 'red', // color de relleno, va a ser transparente
        fillOpacity: 0.0 // opacidad del relleno
    };
};

function createCustomMarker(feature, latlng) {
    
    var bellotaIcon = L.icon({
        iconUrl: 'assets/img/bellotaPI.png',
        iconSize: [38, 33], // size of the icon
    });
    
    return L.marker(latlng, {
        icon: bellotaIcon
    });
    
};

export function createMap(geojson) {

    var map = L.map('map', {
        center: [35.050, -8.090],
        zoom: 5,
        zoomSnap: 0.25,
        zoomControl: false
    });
    
    L.control.zoom({
        position: 'topright'
    }).addTo(map);
    
    var defaultLayer = L.tileLayer.provider('OpenStreetMap.Mapnik').addTo(map);
    
    // Añado el polígono definido al inicio
    var polygon = new L.geoJson(geojsonFeaturePolygon, {
        style: stylePolygon
    }).addTo(map);
    
    
    // Añado los marcadores con sus estilos
    markers = L.markerClusterGroup({});
    L.geoJSON(geojson, {
        pointToLayer: createCustomMarker
    }).addTo(markers);
    
    // si pincho un marker focalizo la latLng y evidencio la card
    markers.addTo(map).on('click', function (e) {
        // evidencia card corespondente
        map.setView(e.latlng)
        
        selectCard(e.layer.feature.properties.id);
    });
    
    // si pincho fuera de un marker quito el hightlight de la card
    map.on('click', function (e) {
        unselectCards();
    })
    
    //AÑADO ESCALA
    var escala = L.control.scale({
        position: 'bottomright',
        imperial: false,
        maxWidth: 200
    });
    map.addControl(escala);
    
    return map;
    
}

// Filtrar por taxonomía
export function filterMarkers(map, geojson, filter, taxonomies) {
    const projects = L.geoJSON(geojson, {
        pointToLayer: createCustomMarker,
        filter: function (feature, layer) {
            
            if (feature.properties.Ciudad == null) {
                return false; //esto no tiene que estar en el mapa
            }
            
            // Creo el objeto proyecto con el identificador de taxonomía (bioregión, ámbito) y el valor
            var project = {}
            project[taxonomies.main.singleId] = feature.properties[taxonomies.main.dbName]
            project[taxonomies.secondary.singleId] = feature.properties[taxonomies.secondary.dbName]
            return filterProjectByTaxonomies(filter,project,taxonomies)
            
        }
    });
    
    markers.clearLayers();
    markers.addLayer(projects);
    var existBounds = Object.keys(markers.getBounds()).length == 2;
    if (existBounds)
    map.fitBounds(markers.getBounds());
    
}


