# Mapa interactivo para mapeo de iniciativas

## Introducción
Mapa interactivo y adaptativo diseñado para el mapeo de iniciativas, basado en OpenStreetMaps y Leaflet. Permite filtrar los resultados en base a dos taxonomías y contiene un buscador simple de texto.

Este proyecto está basado en el código del proyecto [trenzando cuidados](https://www.ecologistasenaccion.org/mapas/mapeoecofem/), la versión actual ha sido desarrollada por [amoved](https://amoved.es).

### Versión escritorio
<img src="./repo-assets/desktop-general-view.png" width="32.9%">
<img src="./repo-assets/desktop-search.png" width="32.9%">
<img src="./repo-assets/desktop-project-detail.png" width="32.9%">

### Versión móvil
<img src="./repo-assets/mobile-legend.png" width="24.5%">
<img src="./repo-assets/mobile-map.png" width="24.5%">
<img src="./repo-assets/mobile-project-detail.png" width="24.5%">
<img src="./repo-assets/mobile-search.png" width="24.5%">

## Arquitectura
El mapa consta de un frontal estático basado en JS puro, Leafleat y OpenStreetMap y se le puede añadir un backend muy sencillo basado en EJS y dockerizado para gestionar los proyectos.

## Desarrollo
Para ampliar las funcionalidades del mapa, descárgate el frontal y en tu máquina local y simplemente levanta un servidor web. Si utilizas VSCode puedes utilizar una extensión como LiveServer (cuidado que puede estar configurado para servir webs en 0.0.0.0 en lugar de 127.0.0.1 por defecto, no es una opción segura).

Las configuraciones principales se pueden hacer en el archivo *src/config.js*. Los proyectos mostrados deben encontrarse en el archivo *db/projects.json* que contendrá un .

## Licencia

Este código se encuentra licencia bajo la licencia [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)